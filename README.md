# extension-manager

A native tool for browsing, installing, and managing GNOME Shell Extensions

https://github.com/mjakeman/extension-manage

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/extension-manager/extension-manager.git
```

